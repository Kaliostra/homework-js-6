"use strict";


//Он переберает все элементы массива один за другим для выполнения функции.
// arrey =[]; либо удалить его присвоить ему null.
//Array.isArrey(arr);

let value = ["Слава", true, "Україні", 2022, {героям: "Слава"}, NaN];
let elementType = "string";
const filterBy = (arrey, type) => arrey.filter(value => typeof value !== type);

console.log(filterBy(value, elementType));